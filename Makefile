BUILDDIR   ?= dist
RUNHASKELL ?= runhaskell Setup --builddir=$(BUILDDIR)

prefix ?= /usr
HFLAGS := -O --enable-shared --enable-executable-dynamic --disable-library-vanilla \
	  --prefix=$(prefix) --docdir=$(prefix)/share/doc/fialka		   \
	  --libsubdir=\$compiler/site-local/\$pkgid -f-lib-only                    \
	  $(HFLAGS)

HMAKEFLAGS := --jobs

config = $(BUILDDIR)/setup-config
bindir = $(BUILDDIR)/build/
executables = fialka

.PHONY: fialka configure test clean install

fialka_files = src/Main.hs src/Commands.hs src/Argparse.hs src/Config.hs

all: fialka

fialka: $(bindir)/fialka/fialka

$(bindir)/fialka/fialka: $(config) $(fialka_files)
	$(RUNHASKELL) $(HMAKEFLAGS) build exe:fialka

configure: $(config)
$(config):
	$(RUNHASKELL) configure $(HFLAGS)

install: fialka
	$(RUNHASKELL) copy --destdir=$(DESTDIR)
	install -Dm644 doc/man/fialka.1 $(DESTDIR)$(prefix)/share/man/man1/fialka.1
	install -Dm644 doc/man/fialka.5 $(DESTDIR)$(prefix)/share/man/man5/fialka.5
	-gzip -f --best $(DESTDIR)$(prefix)/share/man/man1/fialka.1
	-gzip -f --best $(DESTDIR)$(prefix)/share/man/man5/fialka.5
	install -Dm644 contrib/completion/zsh/_fialka \
	    $(DESTDIR)$(prefix)/share/zsh/site-functions/_fialka
	install -Dm644 contrib/completion/bash/fialka \
	    $(DESTDIR)$(prefix)/share/bash-completion/completions/fialka

clean:
	$(RUNHASKELL) clean
