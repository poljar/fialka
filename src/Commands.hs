{-# LANGUAGE OverloadedStrings #-}

module Commands
    ( runDecrypt
    , runEncrypt
    , runShare
    ) where

import Config

import Crypto.Gpgme

import Data.Maybe

import System.Exit
import System.IO
import System.Process.Typed

import qualified Data.ByteString.Char8 as B
import qualified Data.ByteString.Lazy.Char8 as C

getPlainText :: Maybe FilePath -> IO B.ByteString
getPlainText (Just file) = B.readFile file
getPlainText Nothing = B.hGetContents stdin

writeCipherText :: Maybe FilePath -> B.ByteString -> IO ()
writeCipherText (Just file) str = B.writeFile file str
writeCipherText Nothing str = B.putStrLn str

filterFellowship :: String -> Fellowship -> Bool
filterFellowship target (Fellowship name _ _ _) = target == name

findFellowship :: String -> Config -> Maybe Fellowship
findFellowship targetFellowship (Config accs) = listToMaybe fellowships
  where
    fellowships = Prelude.filter (filterFellowship targetFellowship) accs

getFellowshipOrDie :: String -> Config -> IO Fellowship
getFellowshipOrDie targetFellowship conf =
    case fellowship of
        Nothing -> die $ "Fellowship '" ++ targetFellowship ++ "' not found."
        Just a -> return a
  where
    fellowship = findFellowship targetFellowship conf

-- TODO imlement decryption and output color coded signature validity
runDecrypt :: Maybe FilePath -> Maybe FilePath -> IO ()
runDecrypt = undefined

runEncrypt' :: Fellowship -> B.ByteString -> IO Encrypted
runEncrypt' fellowship plainText = do
    -- TODO respect the GPGHOME environment variable
    let gpgHome = "~/.gnupg"
    let locale = "C"
    let pubKeys = fellowshipKeys fellowship
    let toArmor = armor fellowship
    withCtx gpgHome locale OpenPGP $ \context -> do
        setArmor toArmor context
        maybeKeys <- mapM (flip (getKey context) NoSecret) pubKeys
        -- TODO print out which key isn't found
        pubKeys <-
            if any isNothing maybeKeys
                then die "Error: key not found."
                else return (catMaybes maybeKeys)
        -- TODO disable AlwaysTrust and fail gracefully if the key isn't trusted
        -- TODO make AlwaysTrust configurable per fellowship?
        -- TODO do we always want to sing and encrypt?
        eitherEncrypted <- encryptSign context pubKeys AlwaysTrust plainText
        -- TODO make a better error message.
        case eitherEncrypted of
            Left _ -> die "Error encrypting."
            Right val -> return val

runEncrypt :: Config -> String -> Maybe FilePath -> Maybe FilePath -> IO ()
runEncrypt conf fellowship inFile outFile = do
    f <- getFellowshipOrDie fellowship conf
    plainText <- getPlainText inFile
    cipherText <- runEncrypt' f plainText
    writeCipherText outFile cipherText

runShare :: Config -> String -> Maybe FilePath -> IO ()
runShare conf fellowship inFile = do
    f <- getFellowshipOrDie fellowship conf
    let p = shell (uploader f)
    -- TODO some progress message
    plainText <- getPlainText inFile
    cipherText <- runEncrypt' f plainText
    -- TODO check the exit code
    runProcess $ setStdin (byteStringInput $ C.fromStrict cipherText) p
    exitSuccess
