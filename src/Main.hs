{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Argparse
import Commands
import Config

import System.Directory
import System.Exit
import System.FilePath

import Control.Exception

tryGetFile ::
       forall t. (Monoid t)
    => FilePath
    -> (FilePath -> IO t)
    -> IO t
tryGetFile f fileReader = do
    ret <- try $ fileReader f :: IO (Either IOException t)
    case ret of
        Left _ -> mempty
        Right value -> return value

getFile :: FilePath -> (FilePath -> IO (Either t b)) -> (t -> String) -> IO b
getFile f reader errorPrinter = do
    ret <- reader f
    case ret of
        Left err -> die $ errorPrinter err
        Right val -> return val

getConf :: FilePath -> IO Config
getConf f = getFile f readConf confErrorPretty

tryGetConf :: FilePath -> IO Config
tryGetConf file = tryGetFile file getConf

checkFile :: FilePath -> IO FilePath
checkFile f = do
    exists <- doesFileExist f
    if exists
        then return f
        else die $ "No such file: " ++ show f

fromMaybeGlobalOpts :: GlobalOptions -> IO FilePath
fromMaybeGlobalOpts (GlobalOptions mConfFile) = do
    confDir <- getXdgDirectory XdgConfig "fialka"

    case mConfFile of
        Nothing -> return (joinPath [confDir, "config"])
        Just f -> checkFile f

run :: Options -> IO ()
run (Options globOpts cmd) = do
    confFile <- fromMaybeGlobalOpts globOpts
    conf <- tryGetConf confFile
    case cmd of
        Decrypt inFile outFile -> runDecrypt inFile outFile
        Encrypt fellowship inFile outFile ->
            runEncrypt conf fellowship inFile outFile
        Share fellowship inFile -> runShare conf fellowship inFile

main :: IO ()
main = run =<< execArgparse
