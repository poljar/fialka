{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module Config
    ( readConf
    , readFellowship
    , confErrorPretty
    , Fellowships
    , Fellowship(..)
    , Config(..)
    ) where

import Control.Monad.Except
import Data.ConfigFile

import qualified Data.ByteString.Char8 as B
import Data.ByteString.UTF8

newtype Config =
    Config Fellowships
    deriving (Show)

instance Monoid Config where
    mempty = Config []
    mappend (Config a) (Config b) = Config (a ++ b)

type Fellowships = [Fellowship]

data Fellowship = Fellowship
    { fellowshipName :: String
    , fellowshipKeys :: [B.ByteString]
    , uploader :: FilePath
    , armor :: Bool
    } deriving (Show)

readFellowship ::
       MonadError CPError m => ConfigParser -> SectionSpec -> m Fellowship
readFellowship cp s = do
    f <- get cp s "fellowship-keys"
    u <- get cp s "uploader"
    a <-
        if has_option cp s "armor"
            then get cp s "armor"
            else return False
    let keys = B.split ',' $ fromString f
    return (Fellowship s keys u a)

readFellowships ::
       MonadError CPError m => ConfigParser -> [SectionSpec] -> m Fellowships
readFellowships cp = mapM (readFellowship cp)

-- TODO better error messages
confErrorPretty err = "Error parsing conf: " ++ show err

readConf :: MonadIO m => FilePath -> m (Either CPError Config)
readConf file =
    runExceptT $ do
        cp <- join $ liftIO $ readfile emptyCP file
        let s = sections cp
        a <- readFellowships cp s
        return (Config a)
