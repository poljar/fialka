{-# LANGUAGE OverloadedStrings #-}

module Argparse
    ( execArgparse
    , Options(..)
    , GlobalOptions(..)
    , Command(..)
    ) where

import Data.Semigroup ((<>))
import Options.Applicative

type Fellowship = String

data Command
    = Decrypt (Maybe FilePath)
              (Maybe FilePath)
    | Encrypt Fellowship
              (Maybe FilePath)
              (Maybe FilePath)
    | Share Fellowship
            (Maybe FilePath)

newtype GlobalOptions = GlobalOptions
    { confPath :: Maybe FilePath
    }

data Options =
    Options GlobalOptions
            Command

withInfo :: Parser a -> String -> ParserInfo a
withInfo opt desc = info (helper <*> opt) $ progDesc desc

parseOutFile :: Parser (Maybe FilePath)
parseOutFile =
    optional $
    strOption $
    long "output-file" <> short 'o' <> metavar "OUTFILE" <>
    help "Output file to use."

parseInFile :: Parser (Maybe FilePath)
parseInFile =
    optional $
    strOption $
    long "input-file" <> short 'i' <> metavar "INFILE" <>
    help "Input file to encrypt."

parseFellowship :: Parser Fellowship
parseFellowship =
    strArgument $
    metavar "FELLOWSHIP" <> help "Fellowship to use for the command."

parseDecrypt :: Parser Command
parseDecrypt = Decrypt <$> parseInFile <*> parseOutFile

parseEncrypt :: Parser Command
parseEncrypt = Encrypt <$> parseFellowship <*> parseInFile <*> parseOutFile

parseShare :: Parser Command
parseShare = Share <$> parseFellowship <*> parseInFile

parseCommand :: Parser Command
parseCommand =
    subparser $
    command "decrypt" (parseDecrypt `withInfo` "Decrypt a messages.") <>
    command
        "encrypt"
        (parseEncrypt `withInfo`
         "Encrypt a message for a configured fellowship.") <>
    command
        "share"
        (parseShare `withInfo` "Encrypt and upload a message for a fellowship.")

parseOptions :: Parser Options
parseOptions = Options <$> (GlobalOptions <$> parseConfPath) <*> parseCommand

parseConfPath :: Parser (Maybe FilePath)
parseConfPath =
    optional $
    strOption $
    long "config" <> short 'c' <> metavar "FILENAME" <>
    help "Config file to use."

opts :: ParserInfo Options
opts =
    info
        (parseOptions <**> helper)
        (fullDesc <>
         progDesc "Encrypt messages for multiple recipients and share them.")

execArgparse :: IO Options
execArgparse = execParser opts
