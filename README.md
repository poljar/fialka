# fialka
========

- [Intro](#intro)
- [Installation](#installation)
- [Usage](#usage)
- [Requirements](#requirements)

Intro
-----

fialka is a command line utility that enables you to quickly share encrypted
messages with multiple recipients.

Installation
------------

### ArchLinux

TODO Explain PKGBUILD

### Quick stack method

The easiest way to build fialka from source is to use [stack]:

1.  Install [stack].

2.  Change to the fialka source directory and issue the following commands:

        stack setup
        stack install --test

Usage
-----

TODO

Requirements
------------

The requirements can be found in the [cabal](fialka.cabal) file.

[stack]: http://docs.haskellstack.org/en/stable/install_and_upgrade.html
